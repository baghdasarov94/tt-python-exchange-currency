import sys
from pprint import pprint
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from .forms import ExchangeForm
from .services import CurrencyConverter
from var_dump import var_dump


def exchange_view(request):

    if request.method == 'POST':

        form = ExchangeForm(request.POST)

        if form.is_valid():

            converter = CurrencyConverter()
            result = converter.convert(request.POST)

            return render(request, 'exchange_list.html', {
                'form': form,
                'result': result,
            })
    else:
        form = ExchangeForm()

    return render(request, 'exchange_list.html', {'form': form})

