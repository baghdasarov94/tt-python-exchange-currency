from django.urls import path
from exchange import views

app_name = 'exchange'

urlpatterns = [
  path('', views.exchange_view, name='exchange_view'),
]
