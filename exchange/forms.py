from django import forms

CURRENCY = [
    ('CZK', 'Czech koruna'),
    ('EUR', 'Euro'),
    ('PLN', 'Polish złoty'),
    ('USD', 'US dollar'),
]


class ExchangeForm(forms.Form):
    amount = forms.IntegerField(label='Amount', required=1)
    from_currency = forms.CharField(label='From', widget=forms.Select(choices=CURRENCY))
    to_currency = forms.CharField(label='To', widget=forms.Select(choices=CURRENCY))
