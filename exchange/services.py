import json
import urllib.parse
import urllib.request


class CurrencyConverter:
    url = "https://free.currconv.com/api/v7/convert?apiKey=d309c72c1b37d45d914c"

    def convert(self, form):
        key = form['from_currency'] + '_' + form['to_currency']
        data = {'q': key}

        result = urllib.parse.urlencode(data)

        response = urllib.request.Request(self.url + '&' + result)
        data = urllib.request.urlopen(response).read()
        data = json.loads(data.decode('utf-8'))

        result = data['results'][key]
        value = float(result['val']) * float(form['amount'])

        str_result = result['fr'] + ' -> ' + result['to'] + ' = {0} ' .format(value)

        return str_result
