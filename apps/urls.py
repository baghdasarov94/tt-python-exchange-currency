from django.contrib import admin
from django.urls import path, include

from theme import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('exchange.urls', namespace='exchange_page')),
]
